/* global SimpleBar*/

'use strict';

// add method 'closest' for IE11
(function(ELEMENT) {
	ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
	ELEMENT.closest = ELEMENT.closest || function closest(selector) {
		if (!this) return null;
		if (this.matches(selector)) return this;
		if (!this.parentElement) {
			return null;
		} else return this.parentElement.closest(selector);
	};
}(Element.prototype));

// Toggle mob-menu------------
(function() {
	let btn = $('#mob-menu');
	let header = $('.header--main');
	let width = window.innerWidth;
	
	function initClick() {
		btn.off('click').on('click', function() {	
			event.preventDefault();	
			$(this).toggleClass('header__mob-menu--active').next().toggleClass('active');
			header.toggleClass('active');
		});
	
		if (window.innerWidth == width) return;
		width = window.innerWidth;
		btn.removeClass('header__mob-menu--active').next().removeClass('active');
		header.removeClass('active');
	}
	initClick();

	// Resize function
	let doit; 

	function resized() {
		initClick();
	}
	
	window.onresize = function() { 
		clearTimeout(doit); 
		doit = setTimeout(function() { 
			resized(); 
		}, 100); 
	};
}());


// Init select-------------------
(function() {
	let selectShop = $('#select-shop');
	let selectCity = $('#select-city');
	let parentModal = selectCity.parent();

	selectShop.select2({
		minimumResultsForSearch: -1
	});

	selectCity.select2({
		placeholder: 'placeholder',
		dropdownParent: parentModal,
		language: {
			noResults: function () {
				return 'Нет такого города';
			}
		}
	}).on('change.select2', function() {
		let $this = $(this);
		let placeholder = $this.find('.select2-selection__placeholder');
		if (placeholder.length === 0) return $this.addClass('has-value');
		return $this.removeClass('has-value');
	});
}());


// Init scroll-------------------
(function() {
	let wrap = document.querySelectorAll('.table-wrap');
	
	for (let i = 0; i < wrap.length; i++) {
		new SimpleBar(wrap[i], {
			autoHide: false,
			scrollbarMinSize: 50
		});
	}
}());


// Set phones------------------
function setPhones() {
	let table = document.getElementById('table-winners');
	const names = table.getElementsByClassName('winner-name');
	const phones = table.getElementsByClassName('table__td-phone');

	for (let i = 0; i < names.length; i++) names[i].setAttribute('data-phone', phones[i].textContent);
}

if (document.getElementById('table-winners') !== null) setPhones();


// toggle FAQ-------------------
function toggleFaq() {
	let wrap = document.getElementById('faq');

	wrap.onclick = function(e) {
		let target = e.target;
		
		while (target != this) {
			if (target.classList.contains('faq-item__title')) return openFaq(target);
			target = target.parentNode;
		}
	};

	function openFaq(el) {
		el.classList.toggle('active');
		let faq = el.nextElementSibling;

		if (faq.style.maxHeight) faq.style.maxHeight = null;
		else faq.style.maxHeight = faq.scrollHeight + 'px';
	}
}

if (document.getElementById('faq') !== null) toggleFaq();

// Init popup--------------------
function intPopup() {
	$('.js-popup-button').on('click', function(e) {
		e.preventDefault();
		$('.popup').removeClass('js-popup-show');
		let popupClass = '.' + $(this).attr('data-popupShow');
		$(popupClass).addClass('js-popup-show');
		if ($(document).height() > $(window).height()) {
			let scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
			$('html').addClass('no-scroll').css('top', -scrollTop);
			$('body').removeClass('hidden-x');
		}
	});
	closePopup();
}

// Close PopUp
function closePopup() {
	$('.js-close-popup').on('click', function(e) {
		e.preventDefault();
		$('.popup').removeClass('js-popup-show');
		let scrollTop = parseInt($('html').css('top'));
		$('html').removeClass('no-scroll');
		$('body').addClass('hidden-x');
		$('html, body').scrollTop(-scrollTop);
	});

	$('.popup').on('click', function(e) {
		let div = $('.popup__wrap');

		if (!div.is(e.target) && div.has(e.target).length === 0) {
			$('.popup').removeClass('js-popup-show');
			let scrollTop = parseInt($('html').css('top'));
			$('html').removeClass('no-scroll');
			$('body').addClass('hidden-x');
			$('html, body').scrollTop(-scrollTop);
		}
	});
}
intPopup();


// Has value form-------------
(function() {
	const formEl = document.getElementsByClassName('input-wrap__el');

	for (let i = 0; i < formEl.length; i++) checkVal(formEl[i]);

	$('body').on('input', function(e) {
		let target = e.target;
		let formEl = target.closest('.input-wrap__el');
		if (!formEl) return;
		checkVal(target);
	});

	function checkVal(el) {
		return el.value !== '' ? el.classList.add('has-value') : el.classList.remove('has-value');
	}
}());


// Init mask-------------------
(function() {
	$('input[type="tel"]').mask('+7 (000) 000-00-00');
}());


// Validate-------------------
$.each($('form'), function() {
	$(this).validate({
		ignore: [],
		errorClass: 'error',
		validClass: 'success',
		rules: {
			phone: {
				required: true,
				phone: true 
			},
			password: {
				required: true,
				normalizer: function normalizer(value) {
					return $.trim(value);
				} 
			},
			email: {
				required: true,
				email: true 
			},
			name: {
				required: true,
				letters: true 
			},
			surname: {
				required: true,
				letters: true 
			},
			city: {
				required: true,
				normalizer: function normalizer(value) {
					return $.trim(value);
				} 
			},
			message: {
				required: true,
				normalizer: function normalizer(value) {
					return $.trim(value);
				} 
			}
		},
		errorPlacement: function errorPlacement(error, el) {
			$(el).closest('.input-wrap').addClass('error');
		},
		success: function success(error, el) {
			$(el).closest('.input-wrap').removeClass('error');
		}
	});

	jQuery.validator.addMethod('phone', function (value, element) {
		return this.optional(element) || /\+7\s\(\d+\)\s\d{3}-\d{2}-\d{2}/.test(value);
	});

	jQuery.validator.addMethod('letters', function (value, element) {
		return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
	});

	jQuery.validator.addMethod('email', function (value, element) {
		return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
	});
});


// Uplod file-------------
(function() {
	let inputFile = $('#upload-file');

	inputFile.on('change', function() {
		let $this = $(this);
		let fileText = $this.parent().find('.input-wrap__file');
		let fileLength = $this[0].files.length;
		
		return fileLength > 0 ? fileText.text(`Добавлен файл (${fileLength} шт.)`) : fileText.text('Файл не выбран');
	});
}());